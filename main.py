# Este es el archivo principal para hacer funcionar la aplicacion
# Para que funcione importamos la libreria Flask y Api
from flask import Flask
from flask_restx import Api

#Importamos las clases de ContactController y OneContactAction
from controller.contactController import ContactController
from controller.contactController import PostContactController

# Importamos la clase de las variables de entorno
from utils.environment import Environment

app = Flask(__name__)
api = Api(app)

api.add_resource(ContactController, '/contact/<string:ID>')
api.add_resource(PostContactController, '/contact')

if __name__ == '__main__':
    # Instanciamos la clase Enviroment
    env = Environment()
    # Obtenemos las variables de entorno para el proyecto
    config = env.general()
    app.run(port = config['PORT'], debug = True)
