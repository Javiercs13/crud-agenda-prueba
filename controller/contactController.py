#Importamos la libreria RESTX
from flask_restx import Resource

#importamos el archivo que establece la conexion a MongoDB
from utils.conexionMongo import ConexionMongo

#Importamos request para recibir la data de Postman
from flask import request
# CRUD
# La siguiente clase utiliza los metodos
# para acciones sobre varios documentos
class ContactController(Resource):

    conexionMongo = ConexionMongo("agendaCollection")

    #Metodo para LISTAR VARIOS los contactos
    def get(self, ID):
        print('Estos son todos tus contactos: ')
        self.conexionMongo.listarRegistros()
        # self.conexionMongo.mostrarRegistro(ID)
        return f'get {ID}', 200

    # Metodo para ACTUALIZAR en (1) contacto los numeros
    # de celular 1 y 2
    def put(self, ID):
        data = request.get_json()
        myquery=data["myquery"]
        newvalues= {"$set": data["newvalues"]}
        self.conexionMongo.actualizarRegistro(myquery, newvalues)
        return f'put {ID}', 200

    # Metodo para BORRAR (1) contacto
    def delete(self, ID):
        toDelete = request.get_json()
        nombre = toDelete['nombre']
        self.conexionMongo.borrarRegistro(toDelete)
        print(f'Se elimino a {nombre}')
        return f'delete {ID}', 204

class PostContactController(Resource):

    conexionMongo = ConexionMongo("agendaCollection")

    # Metodo para CREAR (1) contacto
    def post(self):
        newContact = request.get_json()
        print('Se agrego el contacto: ', newContact['nombre'])
        print(newContact)
        self.conexionMongo.crearRegistros(newContact)
        return f'post', 201
