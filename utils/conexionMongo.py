# Importamos la libreria de pymongo para
# lograr la conexion a MongoDB
import pymongo
from bson.objectid import ObjectId

# Creamos una clase para establecer el tipo de accion sobre
# la base de datos de MongoDB
class ConexionMongo():

    # En el metodo constructor colocamos en variables
    # el localhost, la base de datos y la coleccion
    def __init__(self, collection):
        myclient = pymongo.MongoClient('mongodb://localhost:27017')
        mydb = myclient["agendaDB"]
        self.mycol = mydb[collection]
        self.db = myclient

    # Metodo para crear un contacto
    # Este recibe como argumento el objeto json
    # del contacto a crear.
    def crearRegistros(self, newDocument):
        x = self.mycol.insert_one(newDocument)
        print(x.inserted_id)

    # Metodo para hacer commit sobre el cambio
    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()

    # Metodo para listar todos los contactos
    def listarRegistros(self):
        for x in self.mycol.find():
            print(x)

    # Metodo de prueba para sacar un registro mediante ID
    def mostrarRegistro(self, ID):
        for x in self.mycol.find_one({'_id': ObjectId(ID)}):
            print(x)

    # Metodo para actualizar un (1) contacto.
    # Este recibe el dato a actualizar como myquery
    # y los valores a cambiar como newvalues
    def actualizarRegistro(self, myquery, newvalues):
        x = self.mycol.update_one(myquery, newvalues)
        print(x.modified_count, "Documento actualizado")

    # Metodo para borrar un (1) contacto.
    # Este recibe como argumento el contacto a borrar
    # como objeto json
    def borrarRegistro(self, myquery):
        x = self.mycol.delete_one(myquery)
        print(x.deleted_count, "Documento borrado")



