import os
from dotenv import load_dotenv

# Creamos una clase donde podemos almacenar variables
# de entorno para utilizarlas en la aplicacion.
class Environment():
    def general(self):
        return {
            'PORT': os.getenv('PORT', '8000'),
        }